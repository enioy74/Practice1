var express = require('express');
var multer  = require('multer');
var member_c = require('../controllers/member.js');
var member = new member_c();
var router = express.Router();

var upload = multer({
  dest : 'member_photo',
  limits : {
    fileSize: 2*1024*1024
  },
  fileFilter : function(req, file, cb) {
    if(file.mimetype === 'image/png')cb(null, true);
    else cb(null, false);
  }
});


router.get('/', member.get_member_data)
      .put('/',multer_err_hanlding,member.update);


module.exports = router;


function multer_err_hanlding(req,res,next){
  upload.single('photo')(req,res,function(err){
    var resobj = {
      message : '',
      error : ''
    }
    if (err && err.code === 'LIMIT_FILE_SIZE') {
      resobj.error = '檔案過大';
      res.json(resobj);
      return;
    }
    next();
  });
}
