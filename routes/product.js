var express = require('express');
var multer  = require('multer');
var product_c = require('../controllers/product.js');
var product = new product_c();
var router = express.Router();

var upload = multer({
  dest : 'product_photo',
});

router.route('/')
      .post(product.user_root,upload.single('photo'),product.add)
      .get(product.get)
      .delete(product.user_root,product.delete);

router.route('/:id')
      .put(product.user_root,upload.single('photo'),product.update);


module.exports = router;
