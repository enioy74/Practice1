var express = require('express');
var visitor_c = require('../controllers/visitor.js');
var visitor = new visitor_c();
var router = express.Router();

router.post('/login', visitor.login)
      .post('/register', visitor.register);


module.exports = router;
