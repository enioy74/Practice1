var express = require('express');
var order = require('../controllers/order.js');

var router = express.Router();
router.route('/')
  .get(order.show)
  .post(order.add);

router.route('/:id')
  .put(order.update)
  .delete(order.delete);

router.route('/pay')
  .post(order.pay);

router.route('/monthtotal')
  .get(order.monthtotal);

module.exports = router;
