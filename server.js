var express = require('express');
var bodyParser = require('body-parser');

var order = require('./routes/order.js');
var member = require('./routes/member.js');
var visitor = require('./routes/visitor.js');
var product = require('./routes/product.js');
var verify = require('./verify.js');
var app = express();


app.use(express.static(__dirname+'/member_photo/'));
app.use(express.static(__dirname+'/product_photo/'));

app.use(bodyParser.urlencoded({ extended: false }));

app.use('/visitor',visitor);

//app.use(verify);

app.use('/member',verify,member);
app.use('/orders',verify,order);
app.use('/product',verify,product);
app.use(verify,function(req,res,next){
    if(req.member.account === 'root')
      next();
    else {
      var resobj ={
        message:'',
        error:''
      };
      resobj.error = '沒有權限操作';
      res.json(resobj);
    }
  },express.static(__dirname+'/monthtotal/'));

app.listen(4000, function () {
    console.log('listening on port 4000!')
});
module.exports = app;
