var nodemailer = require('nodemailer');
var gmail = require('./gmail.js');
var transporter = nodemailer.createTransport({
    service: 'Gmail',
    auth: {
        user: gmail.email,
        pass: gmail.password
    }
});

module.exports = transporter;
