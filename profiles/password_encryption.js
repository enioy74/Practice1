var crypto = require('crypto');
module.exports = function(password){
  var md5 = crypto.createHmac('md5','thepassword');
  md5.update(password);
  return md5.digest('hex');
}
