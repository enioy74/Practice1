var chai = require ('chai');
var chaiHttp = require ('chai-http');
var server = require('../server');
var db = require('../profiles/db');

const roottoken = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhY2NvdW50Ijoicm9vdCIsInBhc3N3b3JkIjoiMTIzNCIsImlhdCI6MTQ5MzM5NDI3MiwiZXhwIjoxNDkzNDgwNjcyfQ.WKBx3CqcZmntP66mrEOyGrfwsC0t8AUldo6B_yV4f1k';
const generaltoken = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhY2NvdW50IjoiYmFhIiwicGFzc3dvcmQiOiJiYiIsImlhdCI6MTQ5MzM5NDQ1MSwiZXhwIjoxNTAyMDM0NDUxfQ.8qlvIQwaBj05JW1ksgxqy7Pf2Kb-sJk2V05rJo56p_w';

chai.should()
chai.use(chaiHttp)

describe('monthtotal is correct or not', function() {
  after(function(done){
    db.query('delete from orders where pay_time="2017-9-30"',function(err,row){
      done()
    });
  })
  describe('no orders month output', function() {
    it('should return 0 total price quentity', function(done) {
      chai.request(server)
        .get('/orders/monthtotal?year=2017&month=9')
        .set('Authorization',roottoken)
        .end(function(err,res){
          res.should.have.status(200)
          res.should.be.a.json        
          res.body.should.be.a('object')
          res.body.should.have.all.key(['message', 'error'])
          res.body.error.should.be.a('string').to.equal('')
          res.body.message.should.be.a('array')
          res.body.message.forEach(function(element) {
            element.should.have.all.key(['name','price','id','salequentity','total'])
            element.salequentity.should.be.a('string').to.equal('0')
            element.total.should.be.a('string').to.equal('0')
          }, this);
          done();
	      })
    });
  });
  describe('orders in last day in month is included', function() {
    it('should return the last order', function(done) {
      db.query('insert into orders values(null,"a",1,2,1,"2017-9-30")',function(err,rows){
        if(err)throw err;
      })
      chai.request(server)
        .get('/orders/monthtotal?year=2017&month=9')
        .set('Authorization',roottoken)
        .end(function(err,res){
          res.should.have.status(200)
          res.should.be.a.json        
          res.body.should.be.a('object')
          res.body.should.have.all.key(['message', 'error'])
          res.body.error.should.be.a('string').to.equal('')
          res.body.message.should.be.a('array')
          res.body.message.forEach(function(element) {
            element.should.have.all.key(['name','price','id','salequentity','total'])
          }, this);
          res.body.message[0].salequentity.should.be.a('string').to.equal('2')
          res.body.message[0].total.should.be.a('string').to.equal('24')
          done();
	      })
      
    });
  });
});

describe('download monthtotal.xlsx with token or not', function() {
  describe('use root token', function() {
    it('should download successful', function(done) {
      chai.request(server)
        .get('/monthtotal.xlsx')
        .set('Authorization',roottoken)
        .end(function(err,res){
          res.should.have.status(200)
          res.type.should.be.a('string').to.equal('application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
          done();
	      })
    });
  });
  describe('use general token', function() {
    it('should return error message', function(done) {
      chai.request(server)
        .get('/monthtotal.xlsx')
        .set('Authorization',generaltoken)
        .end(function(err,res){
          res.should.have.status(200)
          res.should.be.a.json
          res.body.should.be.a('object')
          res.body.should.have.all.key(['message', 'error'])
          res.body.message.should.be.a('string').to.equal('')
          res.body.error.should.be.a('string').to.equal('沒有權限操作')
          done();
	      })
    });
  });
  describe('no use token', function() {
    it('should return error message', function(done) {
      chai.request(server)
        .get('/monthtotal.xlsx')
        .end(function(err,res){
          res.should.have.status(200)
          res.should.be.a.json
          res.body.should.be.a('object')
          res.body.should.have.all.key(['message', 'error'])
          res.body.message.should.be.a('string').to.equal('')
          res.body.error.should.be.a('string').to.equal('沒有權限操作')
          done();
	      })
    });
  });
});

