var express = require('express');
var jwt = require('jsonwebtoken');
var password_encryption = require('./profiles/password_encryption.js');
var jwt_s = require('./profiles/jwtsecret.js');

var app = express();

module.exports =async function(req,res,next){
  var resobj = createresobj();
  var token = req.headers['authorization'];
  try{
    if(!token)throw '沒有權限操作';
    var data = await jwtverify(token);
    req.member = memberobj(data);
    next();
  }catch(err){
    resobj.error = err;
    res.json(resobj);
  }
}


function createresobj(){
  return{
    message:'',
    error:''
  }
}　

function jwtverify(token){
  return new Promise((resolve,reject)=>{
   jwt.verify(token,jwt_s.secret,function(err,decode){
     if(err)reject('token無效');
     else  resolve(decode);
   });
  });
}

function memberobj(data){
  return {
    account : data.account,
    password : password_encryption(data.password)
  }
}
