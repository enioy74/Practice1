var db = require('../db.js');

module.exports = function(id){
  return new Promise((resolve,reject) => {
    db.query('select localphotoname from product where id = ?',[id],function(err,rows){
    	if(err)reject(err);
    	else {
        if(rows.info.numRows == 0)reject('商品不存在');
    	  else resolve(rows);
      }
    });
  });
}
