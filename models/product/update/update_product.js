var db = require('../db.js');

module.exports = function({name,price,quentity,introduction,photo,id}){
  return new Promise((resolve,reject) => {
  　db.query('update product set name = ?,price = ?,quentity = ?,introduction = ?,photoname = ?,localphotoname = ? where id = ?',
    [name,price,quentity,introduction,photo.originalname,photo.filename,id],function(err, rows) {
      if (err){
        if(err.code == 1062)reject('同名商品已存在');
        else reject(err);
      }
      else resolve();
    });
  });
}
