module.exports = {
  add : require('./add_product.js'),
  get : require('./get_product.js'),
  delete : require('./delete_product.js'),
  update : require('./update')
}
