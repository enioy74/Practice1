var db = require('../db.js');

module.exports = function(email){
  return new Promise((resolve,reject) => {
   if(!validateEmail(email)){
     reject('email錯誤');
     return;
   }
  　db.query('select account from customer where email = ?',[email],function(err, rows) {
        if (err)reject(err);
	else {
	  if(rows.info.numRows > 0)reject('信箱已被使用');
	  else resolve();	
	}
   });
  });
}

function validateEmail(email) {
  reg = /^[^\s\.\@]+@[^\s\.\@]+\.[^\s\.\@]{1,3}$/;
  return reg.test(email) ? true:false;
} 

