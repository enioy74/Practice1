var db = require('../db.js');

module.exports = function({account,name,password,photo}){
  return new Promise((resolve,reject) => {  
    db.query('update customer set password = ?,name = ?,photoname = ?,localphotoname = ? where account = ?',
    [password,name,photo.originalname,photo.filename,account],function(err,rows){
    	if(err)reject(err);
    	else resolve();
    });
  });
}
