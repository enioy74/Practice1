var db = require('../db.js');
module.exports = function(account){
  return new Promise((resolve,reject) => {
    db.query('select localphotoname from customer where account = ?',[account],function(err,rows){
    	if(err)reject(err);
    	else {
        if(rows.info.numRows == 0)reject('用戶不存在');
    	  else resolve(rows);
      }
    });
  });
}
