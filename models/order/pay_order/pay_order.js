var db = require('../db.js');
module.exports = function(quentity_data,account){
  return new Promise((resolve,reject) => {
      transaction();
      var i = quentity_data.length - 1;
      db.query('update product set quentity = ? where id = ?',
      [quentity_data[i].product_quentity,quentity_data[i].productid],
      function callback(err,rows){
        if(err){
          rollback();
          reject(err);
          return;
        }

          if(i>0){
            i--;
            db.query('update product set quentity = ? where id = ?',
            [quentity_data[i].product_quentity,quentity_data[i].productid],callback);
          }
          else{
            var date = getdate();
            db.query('update orders set complete = 1,pay_time = ? where customer = ? and complete = 0',
            [date,account],function(err,rows){
              if(err){
                rollback();
                reject(err);
              }
              else{
                commits();
                resolve();
              }
            });
          }

      });

  });
}



function rollback(){
  db.query('rollback;');
}
function commits(){
  db.query('commit;');
}
function transaction(){
  db.query('start transaction;');
}

function getdate(){
  var time = new Date();
  var year = time.getFullYear(),
  month = time.getMonth()+1,
  day = time.getDate(),
  hour = time.getHours(),
  minute = time.getMinutes(),
  second = time.getSeconds();
  return year + '-' + month + '-' + day + ' ' + hour + ':' + minute + ':' + second;
}
