module.exports = {
  show_order : require('./show_order.js'),
  add_order : require('./add_order.js'),
  update_order : require('./update_order.js'),
  month_total : require('./month_total.js'),
  delete_order : require('./delete_order/delete_order.js'),
  complete_confirm : require('./delete_order/complete_confirm.js'),
  show_pay_order : require('./pay_order/show_pay_order.js'),
  get_email : require('./pay_order/get_email.js'),
  pay_order : require('./pay_order/pay_order.js'),
  mail_content : require('./pay_order/mail_content.js')
}
