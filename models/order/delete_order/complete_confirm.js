var db = require('../db.js');
module.exports = function(orderid){
  return new Promise((resolve,reject) => {
    db.query('select complete from orders where id = ?',[orderid],function(err,rows){
      if(err)reject(err);
      else resolve(rows);
    });
  });
}
