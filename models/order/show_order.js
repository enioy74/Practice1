var db = require('./db.js');
module.exports = function(account){
  return new Promise((resolve,reject) => {
    db.query(getquerystring(account),[account],function(err,rows){
      if(err)reject(err);
      else resolve(rows);
    });
  })
}

function getquerystring(account){
  return getselectstring()+getwherestring(account);
}
function getselectstring(){
  return 'select a.*,b.name,b.price from orders a,product b';
}
function getwherestring(account){
  return account ? ' where a.productid = b.id and a.customer = ? and complete = 0'
                  :' where a.productid = b.id';
}
