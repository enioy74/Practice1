var fs = require('fs');
var model = require('../models/product');

module.exports = class product{

  user_root(req,res,next){
    if(req.member.account === 'root')
      next();
    else {
      var resobj = createresobj();
      resobj.error = '無權限';
      res.json(resobj);
    }
  }

  async get(req,res){
    var resobj = createresobj();
    var page = req.query.page;
    try{
      pagedetect(page)
      var rows = await model.get(page);
      resobj.message = rows;
      res.json(resobj);
    }catch(err){
      resobj.error = err;
      res.json(resobj);
    }
  }

  async add(req,res){
    var resobj = createresobj();
    var product = {
      name : req.body.name,
      price : req.body.price,
      quentity : req.body.quentity,
      photo : req.file,
      introduction : req.body.introduction
    }

    try {
      parameter_detect(product);
      add_file_format(product.photo);
      await model.add(product);
      resobj.message = '新增成功';
      res.json(resobj);
    } catch (err) {
      delete_file(product.photo.filename);
      resobj.error = err;
      res.json(resobj);
    }
  }

  async update(req,res){
    var resobj = createresobj();
    var product = {
      name : req.body.name,
      price : req.body.price,
      quentity : req.body.quentity,
      photo : req.file,
      introduction : req.body.introduction,
      id : req.params.id
    }

    try {
      parameter_detect(product);
      var rows = await model.update.select_localphoto(product.id);
      add_file_format(product.photo);
      await model.update.update_product(product);
      delete_file(rows[0].localphotoname);
      resobj.message = '更新成功';
      res.json(resobj);
    } catch (err) {
      delete_file(product.photo.filename);
      resobj.error = err;
      res.json(resobj);
    }
  }

  async delete(req,res){
    var resobj = createresobj();
    var product = {
      id : req.body.id,
    }

    try {
      await model.delete(product);
      resobj.message='刪除成功';
      res.json(resobj);
    } catch (err) {
      resobj.error = err;
      res.json(resobj);
    }
  }
}



function createresobj(){
  return{
    message:'',
    error:''
  }
}　

function pagedetect(page){
  if(page <= 0)throw '沒有資料';
}
//add&update
//檢測參數是否為空
function parameter_detect(product){
  if(!(product.name && product.price && product.quentity && product.photo)){
    throw '必填參數不能空值';
  }
}
//加入檔案格式
function add_file_format(file){
  var fileformat;
  if(file.mimetype === 'image/png'){
    fileformat='.png';

    fs.rename(file.path,file.path+fileformat,function(err){
      if (err) throw err;
    });
    file.filename += fileformat;
    file.path += fileformat;
  }
}
//刪除接收到已經放置資料夾內的圖檔
// function delete_last_file(photo){
//   if(photo){
//     fs.unlink(photo.path,function(err){
//       if(err) {
//         console.log(err);
//       }
//       console.log('file deleted successfully');
//     });
//   }
// }

function delete_file(localphotoname){
  if(localphotoname){
    fs.unlink('./product_photo/'+localphotoname,function(err){
      if(err) {
        console.log(err);
      }
      else console.log('file deleted successfully');
    });
  }
}
