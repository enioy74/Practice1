var express = require('express');
var fs = require('fs');
var model = require('../models/member');

module.exports = class member{

  async get_member_data(req,res){
    var resobj = createresobj();
    var member = {
      account : req.member.account,
    }
    try{
      var rows = await model.get_member_data(member.account);
      resobj.message = rows[0];
      //res.json(resobj);
       res.render('memberdata.ejs',{member:rows[0]});//回傳頁面
    }catch(err){
      resobj.error = err;
      res.json(resobj);
    }
  }

  async update(req,res){
    var resobj = createresobj();
    var member = {
      account : req.member.account,
      name : req.body.name,
      password : req.body.password,
      photo : req.file
    }

    try {
      if(!member.photo)throw '檔案格式錯誤';
      if(!member.password)throw '密碼不能為空';
      if(!member.name)throw '名字不能為空';
      add_file_format(member.photo);
      var rows = await model.update.select_localphoto(member.account);
      delete_file(rows[0].localphotoname);
      await model.update.update_member_data(member);
      resobj.message = '更新成功';
      res.json(resobj);
    } catch (err) {
      resobj.error = err;
      res.json(resobj);
    }
  }

}


function createresobj(){
  return{
    message:'',
    error:''
  }
}　

//加入檔案格式
function add_file_format(file){
  var fileformat;
  if(file.mimetype === 'image/png'){
    fileformat='.png';
  }
  fs.rename(file.path,file.path+fileformat,function(err){
    if (err) throw err;
  });
  file.filename+=fileformat;
}
//刪除原來圖片
function delete_file(localphotoname){
  if(localphotoname != null){
    fs.unlink('./member_photo/'+localphotoname,function(err){
      if(err) {
        console.log(err);
      }
      console.log('file deleted successfully');
    });
  }
}
