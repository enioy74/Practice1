var express = require('express');
var jwt = require('jsonwebtoken');
var model = require('../models/visitor');
var jwt_s = require('../profiles/jwtsecret.js');
var password_encryption = require('../profiles/password_encryption.js');


module.exports = class visitor{

  async register(req,res){
    var resobj = createresobj();
    var member = {
      account : req.body.account,
      password : req.body.password,
      email : req.body.email
    }
    try{
      member.password = password_detect(member.password);
      await model.register.account_detect(member.account);
      await model.register.email_detect(member.email);
      resobj.message = await model.register.register(member);
      res.json(resobj);
    }catch(err){
      resobj.error = err;
      res.json(resobj);
    }
  };

  async login(req,res){
    var resobj = createresobj();
    var member = {
      account : req.body.account,
      password : req.body.password
    }
    try{
      var rows = await model.login_get_password(member.account);
      account_exist(rows);
      var password_e = password_encryption(member.password);
      password_confirm(rows,password_e);
      var token = jwt.sign(member,jwt_s.secret,{expiresIn:'100d'});
      res.setHeader('Authorization',token);
      resobj.message = '登錄成功';
      res.json(resobj);
    }catch(err){
      resobj.error = err;
      res.json(resobj);
    }

  };

}



function createresobj(){
  return{
    message:'',
    error:''
  }
}　

function password_detect(password){
  if(!password)throw '密碼不能為空';
  else return password_encryption(password);
}

function account_exist(rows){
  if(rows.length == 0)
    throw '帳號錯誤';
}
function password_confirm(rows,password_e){
  if(rows[0].password !== password_e)
    throw '密碼錯誤';
}
