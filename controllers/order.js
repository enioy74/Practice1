var express = require('express');
var excelbuilder = require('msexcel-builder');
var model = require('../models/order/index.js');
var transporter = require('../profiles/sendmail.js');

module.exports.show = function(req,res){//訂單列表
    var resobj = createresobj();
    var account = req.query.account;
    model.show_order(account).then(
      (rows)=>{
        if(rows.length!=0){
          resobj.message = rows;
        }
        res.json(resobj);
      }
    );
  };//先測有無帳號在show　　或單純找傳過來的帳號有無帳單

module.exports.add = function(req,res){//新增訂單
    var resobj = createresobj();
    var orderdata = {
      account : req.body.account,
      productid : req.body.productid,
      quentity : req.body.quentity
    }
    model.add_order(orderdata).then(
      (rows) => {
        resobj.message='新增成功';
        res.json(resobj);
      },
      (err) => {
        resobj.error = err;
        res.json(resobj);
      }
    );
  };

module.exports.update = function(req,res){//修改訂單
    var resobj = createresobj();
    var orderdata = {
      orderid : req.params.id,
      productid : req.body.productid,
      quentity : req.body.quentity
    }
    model.update_order(orderdata).then(
      (rows) => {
        resobj.message='修改成功';
        res.json(resobj);
      },
      (err) => {
        updateerror(resobj,err);
        res.json(resobj);
      }
    );
  };
module.exports.delete = function(req,res){//刪除單筆訂單 查查exist
    var resobj = createresobj();
    var orderid = req.params.id;
    model.complete_confirm(orderid).then(
      (rows) => {
        payornot({rows,resobj,orderid});
        res.json(resobj);
      }
    );
  };


module.exports.pay = function(req,res){//付款　先選擇訂單　找出存貨　扣除存貨 complete=1 且mail
    var resobj = createresobj();
    var account = req.body.account;
    model.show_pay_order(account).then(//訂單查找與存貨訂貨比對
      (rows) => {
        resobj.error = orderresult(rows);
        if(resobj.error === ''){
          return rows;
        }
        else{
          res.json(resobj);
        }
      }
    ).then(
      (rows) => {
        if(!rows)return;
        //model.quentitydeduct(rows,req);//減少庫存
        model.pay_order(rows,account).then(//cmoplete=1
          () => {
            var email = new model.mail_content();
            model.get_email(account).then(
              (mail) => {
                email.to = mail[0].email;
                email.text = emailtext(rows);
                transporter.sendMail(email, sendmailmessage);
                resobj.message = '付款成功　請至信箱確認';
                res.json(resobj);
              }
            );
          },
          (err) => {
            resobj.error = '付款失敗';
            res.json(resobj);
          }
        );
    });
  };

module.exports.monthtotal = function(req,res){
    var resobj = createresobj();
    var time = {
      year : req.query.year,
      month : req.query.month
    }
    model.month_total(time).then(
      (rows) => {
        makexls(rows);
        resobj.message = rows;
        res.json(resobj);
      },
      (err) => {
        resobj.error = err;
        res.json(resobj);
      }
    );
  };


function makexls(row){
  var workbook = excelbuilder.createWorkbook('./monthtotal', 'monthtotal.xlsx');
  var sheet1 = workbook.createSheet('sheet1', 5, row.length+1);
  sheet1.set(1, 1, 'name');
  sheet1.set(2, 1, 'price');
  sheet1.set(3, 1, 'id');
  sheet1.set(4, 1, 'salequentity');
  sheet1.set(5, 1, 'total');

  for(let i=0;i<row.length;i++){
    sheet1.set(1, i+2, row[i].name);
    sheet1.set(2, i+2, row[i].price);
    sheet1.set(3, i+2, row[i].id);
    sheet1.set(4, i+2, row[i].salequentity);
    sheet1.set(5, i+2, row[i].total);
  }
  workbook.save(function(ok){
    if (!ok) 
      workbook.cancel();
    else
      console.log('congratulations, your workbook created');
  });
}

function createresobj(){
  return{
    message:'',
    error:''
  }
}　
//有無付款的判斷決定回應與處理方式　0還沒付款　刪除　1已付　不能刪　2無訂單　
function payornot({rows,resobj,orderid}){
  if(rows.info.numRows != 0){
    if(rows[0].complete == 0){
      model.delete_order(orderid);//
      resobj.message = '刪除成功';
    }
    else if(rows[0].complete == 1){
      resobj.error = '訂單已付款';
    }
  }
  else{
    resobj.error = '無此訂單';
  }
}
//付款前先檢查訂單的結果
function orderresult(rows){
  if(rows.info.numRows == 0){
    return '沒有訂單支付';
  }
  var id = quentitygap(rows);
console.log(rows)
  if(id.length != 0){//存貨不足
    var message = id+'已無存貨'
    return message;
  }
  else{
    return '';
  }
}
//訂單與存貨的數量差
function quentitygap(rows){
  var id = [];
  rows.forEach((row) =>{
    row.product_quentity -= row.quentity;
    if(row.product_quentity < 0){
      id.push(row.productid);
    }
  });
  return id;
}

//付款確認內容
function emailtext(rows){
  var text = '';
  for (var i = 0; i < rows.length; i++) {
    var name = 'product:' + rows[i].name;
    var quentity = '\nquentity:' + rows[i].quentity;
    var price = '\nprice:' + rows[i].price;
    var totalprice = '\ntotalprice:' + rows[i].price*rows[i].quentity;
    var line = '\n\n';
    text += name + quentity + price + line + totalprice;
  }
  return text;
}

function sendmailmessage(error, info){
  if(error){
    console.log(error);
  }
  else{
    console.log('訊息發送: ' + info.response);
  }
}

function updateerror(resobj,err){
return err === '無法更改訂單'　? resobj.error = err:resobj.error = '沒有此商品';
}
